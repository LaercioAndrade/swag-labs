/// <reference types="Cypress"/>
import loginElements from '../elements/loginElements'
const loginelements = new loginElements
const url = Cypress.config("baseUrl")

class LoginPage {

//acessarSite
    acessarSite(){
        cy.visit(url)
        cy.wait(1000)
    }

    username(){
        cy.get(loginelements.username()).type("standard_user")
    }

    password(){
        cy.get(loginelements.password()).type("secret_sauce")
    }

    login(){
        cy.get(loginelements.buttonlogin()).click()
    }

    validarlogin(){
        cy.get(loginelements.validarlogin()).contains("Products")
        cy.screenshot()
    }


}

export default LoginPage;