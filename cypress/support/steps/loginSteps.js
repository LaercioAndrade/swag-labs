/*global Given, Then, When*/

import LoginPage from '../pageobjects/loginPage'
const PageLogin = new LoginPage 


Given("que eu acesso o site Swag Labs", () => {
    PageLogin.acessarSite();
})

When("preencho o login e senha válidos", () => {
    PageLogin.username();
    PageLogin.password();
    PageLogin.login();
})

Then("valido se o login é realizado com sucesso", () => {
    PageLogin.validarlogin();
})